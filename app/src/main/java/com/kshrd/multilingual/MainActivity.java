package com.kshrd.multilingual;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

public class MainActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setDefaultLanguage("vi");
        setContentView(R.layout.activity_main);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.item_lang, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.lang_en:
                setLanguage("en");
                break;
            case R.id.lang_km:
                setLanguage("km");
                break;
            case R.id.lang_zh:
                setLanguage("zh");
                break;
            case R.id.lang_vi:
                setLanguage("vi");
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
